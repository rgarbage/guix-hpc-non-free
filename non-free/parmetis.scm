;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; However, note that this module provides packages for "non-free" software,
;;; which denies users the ability to study and modify it.  These packages
;;; are detrimental to user freedom and to proper scientific review and
;;; experimentation.  As such, we kindly invite you not to share it.
;;;
;;; Copyright © 2021, 2023 Inria

(define-module (non-free parmetis)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpi)
  #:use-module ((guix licenses) #:prefix license:))

(define-public parmetis
  (package
    (name "parmetis")
    (version "4.0.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "http://glaros.dtc.umn.edu/gkhome/fetch/sw/parmetis/"
                           "parmetis-" version ".tar.gz"))
       (sha256
        (base32
         "0pvfpvb36djvqlcc3lq7si0c5xpb2cqndjg8wvzg35ygnwqs5ngj"))))
    (build-system cmake-build-system)
    (native-inputs (list openmpi))
    (arguments
     `(#:tests? #f                      ;no tests
       #:configure-flags `("-DSHARED=ON"
                           ,"-DCMAKE_C_COMPILER=mpicc"
                           ,"-DCMAKE_CXX_COMPILER=mpic++"
                           ,"-DCMAKE_VERBOSE_MAKEFILE=1"
                           ,(string-append "-DGKLIB_PATH=../parmetis-"
                                           ,(package-version parmetis) "/metis/GKlib")
                           ,(string-append "-DMETIS_PATH=../parmetis-"
                                           ,(package-version parmetis) "/metis"))))
    (home-page "http://glaros.dtc.umn.edu/gkhome/metis/parmetis/overview")
    (synopsis "Parallel graph partitioning and fill-reducing matrix ordering")
    (description
     "ParMETIS is an MPI-based parallel library that implements a variety of algorithms
      for partitioning unstructured graphs, meshes, and for computing fill-reducing
      orderings of sparse matrices.  ParMETIS extends the functionality provided
      by METIS and includes routines that are especially
      suited for parallel AMR computations and large scale numerical simulations.
      The algorithms implemented in ParMETIS are based on the parallel multilevel k-way
      graph-partitioning, adaptive repartitioning, and parallel multi-constrained
      partitioning schemes developed in our lab.")
    ;; see license description at http://glaros.dtc.umn.edu/gkhome/metis/parmetis/download
    (license #f)))

(define-public parmetis-i64
  (package/inherit parmetis
    (name "parmetis-i64")
    (arguments
     (substitute-keyword-arguments (package-arguments parmetis)
       ((#:phases phases #~%standard-phases)
	#~(modify-phases #$phases
	    (add-before 'configure 'change-idx-type
	      (lambda _
                (substitute* "metis/include/metis.h"
                  (("define IDXTYPEWIDTH 32") "define IDXTYPEWIDTH 64"))))))))
    (synopsis
     "Parallel graph partitioning and fill-reducing matrix ordering (64-bit indexes)")))

(define-public metis4parmetis-i64
  (package
    ;; This is METIS (which is free software), but taken from the ParMETIS
    ;; tarball to make sure it's consistent.
    (inherit parmetis-i64)
    (name "metis4parmetis-i64")
    (inputs (list openblas))
    (arguments
     (list #:configure-flags
           #~`("-DSHARED=ON"
               ,(string-append "-DGKLIB_PATH="
                               (getcwd) "/parmetis-"
                               #$(package-version this-package)
                               "/metis/GKlib"))
           #:tests? #f
           #:phases #~(modify-phases %standard-phases
                        (add-before 'configure 'chg-idx-type
                          (lambda _
                            (substitute* "metis/include/metis.h"
                              (("define IDXTYPEWIDTH 32")
                               "define IDXTYPEWIDTH 64"))))
                        (add-before 'configure 'rm-build
                          (lambda _
                            (delete-file-recursively "./build")))
                        (add-before 'configure 'change-directory
                          (lambda _
                            (chdir "metis"))))))
    (license license:asl2.0)))

(define-public superlu-dist-64
  (let ((version "6.2.0")
        (revision "0")
        ;; XXX: Do we really need this specific commit?
        (commit "0f6efc377df2440c235452d13d28d2c717f832a1"))
    (package
      (inherit superlu-dist)
      (name "superlu-dist-64")
      (version (git-version version revision commit))
      (source (origin (method git-fetch)
                      (uri (git-reference
                            (url "https://github.com/xiaoyeli/superlu_dist")
                            (commit commit)))
                      (file-name (git-file-name name version))
                      (sha256
                       (base32
                        "0kl6ai30zblb4fyvz3p1qrmra4zvml277j2d89g4glqwjv187i50"))))
      (build-system cmake-build-system)
      (inputs (list gfortran openblas lapack))
      (propagated-inputs (list openmpi metis4parmetis-i64 parmetis-i64))
      (arguments
       (list #:configure-flags #~`("-DBUILD_SHARED_LIBS:BOOL=YES"
                                   "-DTPL_BLAS_LIBRARIES=-lopenblas"
                                   "-DTPL_LAPACK_LIBRARIES=-llapack"
                                   ,(string-append "-DTPL_PARMETIS_LIBRARIES="
                                                   #$(this-package-input "parmetis-i64")
                                                   "/lib/libparmetis.so;"
                                                   #$(this-package-input "metis4parmetis-i64")
                                                   "/lib/libmetis.so")
                                   ,(string-append "-DTPL_PARMETIS_INCLUDE_DIRS="
                                                   #$(this-package-input "parmetis-i64")
                                                   "/include;"
                                                   #$(this-package-input "metis4parmetis-i64")
                                                   "/include")
                                   "-Denable_openmp=ON"
                                   "-DXSDK_INDEX_SIZE=64"
                                   "-DCMAKE_C_STANDARD=99")
             #:phases
             #~(modify-phases %standard-phases
                 (add-before 'check 'mpi-setup #$%openmpi-setup)
                 (add-before 'check 'omp-setup
                   (lambda _ (setenv "OMP_NUM_THREADS" "1"))))))
      (synopsis "Parallel supernodal direct solver (with ParMETIS dependency)"))))
