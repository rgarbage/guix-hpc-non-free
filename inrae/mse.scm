;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2023 Inria

(define-module (inrae mse)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (gnu packages algebra)
  #:use-module (guix packages)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages check)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages ssh)
  #:use-module (non-free parmetis)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages simulation)
  #:use-module (gnu packages swig))

(define-public mse
  (package
	(name "mse")
	(version "v1.0")
	(home-page "https://forgemia.inra.fr/olivier.bonnefon/mse/")
	(source (origin 
	  (method url-fetch)
	  (uri (string-append "https://forgemia.inra.fr/olivier.bonnefon/mse/-/archive/" version "/mse-" version ".tar.gz" ))
		(sha256 (base32 "1ghpzl826855lb8v8lffbjzbd26kk1snyby335riz2nhbky139pk"))))
	(build-system cmake-build-system)
	(arguments
     		(list
      		#:configure-flags #~`("-DBUILD_SHARED_LIBS=ON"
				  ,(string-append "-DPETSC_INCLUDE_DIRS="
					#$(this-package-input
                                        "python-petsc4py") "/include"))
                #:validate-runpath? #f ;;; find why we need that
      			#:phases #~(modify-phases %standard-phases
                   	(add-before 'configure 'chdir-to-trunk/src
                     		(lambda _
                       		(chdir "trunk/src"))))))
    	(synopsis "environnement mecha stat")
	(native-inputs (list pkg-config swig python-wheel))
	(inputs (list openmpi openssh-sans-x python python-petsc4py petsc-openmpi dolfinx python-numpy python-ipython python-fenics-ufl python-fenics-basix python-dolfinx python-petsc4py fenics-ffcx python-mpi4py openmpi gmsh python-meshio python-fenics-ffcx python-h5py))
	(description "simulation et estimations d'edps")
	(license license:lgpl3)
	))

(define-public python-fenics-ufl
  (package
    (name "python-fenics-ufl")
    (version "2023.1.0")
    (home-page "https://github.com/FEniCS/ufl.git")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0nd02wp8v557xisgyajvdk5idhp54swwzcdb6pvqglvnh2zf0d10"))))
    (build-system pyproject-build-system)
    (native-inputs (list python-pytest))
    (inputs (list python-numpy))
    (synopsis "UFL is part of the FEniCS Project")
    (description
     "The Unified Form Language (UFL) is a domain specific language for declaration
      of finite element discretizations of variational forms.  More precisely, it
      defines a flexible interface for choosing finite element spaces and
      defining expressions for weak forms in a notation close to mathematical notation.")
    (license license:lgpl3)))

(define-public fenics-basix
  (package
    (name "fenics-basix")
    (version "0.6.0")
    (home-page "https://github.com/FEniCS/basix.git")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "14g3hxjzqa8f1kc9jgnpclr8isd1qky2vpy0afky5aih6gd2mfad"))))
    (build-system cmake-build-system)
    (arguments
     (list
      #:configure-flags #~`("-DBUILD_SHARED_LIBS=ON")
      #:tests? #f
      #:phases #~(modify-phases %standard-phases
                   (add-before 'configure 'chdir-to-cpp
                     (lambda _
                       (chdir "cpp"))))))
    (inputs (list openblas))
    (synopsis
     "Basix is one of the components of FEniCSx, alongside UFL, FFCx, and DOLFINx")
    (description
     "Basix is a finite element definition and tabulation runtime library.")
    (license license:lgpl3)))

(define-public python-fenics-basix
  (package/inherit fenics-basix
    (name "python-fenics-basix")
    (build-system pyproject-build-system)
    (arguments (list #:tests? #f))
    (native-inputs (list cmake))
    (inputs (modify-inputs (package-inputs fenics-basix)
              (append openblas)
              (append fenics-basix)
              (append python-wheel)
              (append pybind11)
              (append python-numpy)
              (append python-scikit-build)))))

(define-public fenics-ffcx
  (package
    (name "fenics-ffcx")
    (version "0.6.0")
    (home-page "https://github.com/FEniCS/ffcx.git")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1q7kgbnx9nzqv02lsipy64ikxp8i72bahhjmxcwy3s4l5jy7fvh2"))))
    (build-system cmake-build-system)
    (arguments
     (list
      #:configure-flags #~`("-DBUILD_SHARED_LIBS=ON")

      #:tests? #f
      #:phases #~(modify-phases %standard-phases
                   (add-before 'configure 'chdir-to-cmake
                     (lambda _
                       (chdir "cmake"))))))
    (inputs (list python))
    (synopsis "FFCx is a new version of the FEniCS Form Compiler")
    (description
     "FFCx is a compiler for finite element variational forms.  From a
     high-level description of the form in the Unified Form
     Language (UFL), it generates efficient low-level C code that can
     be used to assemble the corresponding discrete operator (tensor).")
    (license license:lgpl3)))

(define-public python-fenics-ffcx
  (package/inherit fenics-ffcx
    (name "python-fenics-ffcx")
    (build-system pyproject-build-system)
    (arguments (list #:tests? #f))
    (inputs (modify-inputs (package-inputs fenics-ffcx)
              (append python-fenics-basix)
              (append python-fenics-ufl)
              (append python-cffi)
              (append python-numpy)
              (append fenics-ffcx)))))

(define-public dolfinx
  (package
    (name "dolfinx")
    (version "0.6.0")
    (home-page "https://github.com/FEniCS/dolfinx")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1sarc8f5mwfq3f0mky14b1vaczs0v8dj49jsy847v8ssz03isj1m"))))
    (build-system cmake-build-system)
    (arguments
     (list
      #:configure-flags #~`("-DBUILD_SHARED_LIBS=ON"
                            "-DDOLFINX_UFCX_PYTHON=OFF"
                            "-DDOLFINX_ENABLE_ADIOS2=OFF"
                            "-DDOLFINX_SKIP_BUILD_TESTS=ON"
                            "-DDOLFINX_ENABLE_SLEPC=OFF"
                            ,(string-append "-DUFCX_INCLUDE_DIR="
                                            #$(this-package-input
                                               "fenics-ffcx") "/include")
                            ,(string-append "-DSCOTCH_ROOT="
                                            #$(this-package-input "pt-scotch"))
                            ,(string-append "-DPARMETIS_ROOT="
                                            #$(this-package-input "parmetis")))
      #:tests? #f ;because we do not know how to use
      #:phases #~(modify-phases %standard-phases
                   (add-before 'configure 'chdir-to-cpp
                     (lambda _
                       (chdir "cpp"))))))
    (native-inputs (list pkg-config python-pytest))
    (inputs (list openmpi
                  scotch
                  zlib
                  pt-scotch
                  parmetis
                  python-mpi4py
                  python-numpy
                  metis
                  hdf5-parallel-openmpi
                  python
                  pugixml
                  petsc-openmpi
                  boost
                  fenics-basix
                  fenics-ffcx))
    (synopsis "New version of DOLFIN and is being actively developed")
    (description
     "DOLFINx is the computational environment of FEniCSx and
     implements the FEniCS Problem Solving Environment in C++ and Python.")
    (license license:lgpl3)))

(define-public python-dolfinx
  (package/inherit dolfinx
    (name "python-dolfinx")
    (build-system python-build-system)
    (arguments (list #:tests? #f ;because we do not know how to use
                     #:phases #~(modify-phases %standard-phases
                                  (add-before 'build 'chdir-to-python
                                    (lambda _
                                      (chdir "python"))))))
    (native-inputs (modify-inputs (package-native-inputs dolfinx)
                     (append cmake)))
    (inputs (modify-inputs (package-inputs dolfinx)
              (append python-petsc4py openssh-sans-x)
              (append python-fenics-basix python-fenics-ufl python-fenics-ffcx)
              (append python-cffi pybind11)
              (append dolfinx)))))



