;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Note that this module provides packages that depend on "non-free"
;;; software, which denies users the ability to study and modify it.
;;;
;;; Copyright © 2019, 2022, 2023 Inria

(define-module (inria tainted solverstack)
  #:use-module (guix)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system python)
  #:use-module (guix git)                         ;for 'git-checkout'
  #:use-module (gnu packages)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages maths)
  #:use-module (guix utils)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-xyz)
  #:use-module (hacky mumps-mkl)
  #:use-module (inria solverstack)
  #:use-module (inria storm)
  #:use-module (inria tadaam)
  #:use-module (guix-science-nonfree packages cuda)
  #:use-module (guix-science-nonfree packages mpi)
  #:use-module (non-free mkl)
  #:use-module (inria tainted storm)
  #:use-module (tainted python-xyz))

(define-public chameleon+cuda
  (package
   (inherit chameleon)
   (name "chameleon-cuda")
   (arguments
    (substitute-keyword-arguments (package-arguments chameleon)
                                  ((#:configure-flags flags '())
                                   `(cons "-DCHAMELEON_USE_CUDA=ON" ,flags))))
   (native-inputs
    (modify-inputs (package-native-inputs chameleon)
                   (prepend ;; ("gcc" ,gcc-8)                             ;CUDA requires GCC <= 8
                    )))
   (propagated-inputs
    (modify-inputs (package-propagated-inputs chameleon)
                   (prepend cuda starpu+cuda openmpi-cuda)
                   (delete "starpu")
                   (delete "openmpi")))))

(define-public chameleon+cuda+nompi
  (package
   (inherit chameleon+cuda)
   (name "chameleon-cuda-nompi")
   (arguments
    (substitute-keyword-arguments (package-arguments chameleon+cuda)
                                  ((#:configure-flags flags '())
                                   `(delete "-DCHAMELEON_USE_MPI=ON" ,flags))))))

(define-public dplasma+cuda
  (package
   (inherit dplasma)
   (name "dplasma-cuda")
   (propagated-inputs
    (modify-inputs (package-propagated-inputs dplasma)
                   (prepend cuda openmpi-cuda)
                   (delete "openmpi")))))

(define-public chameleon+mkl+mt
  (package
   (inherit chameleon)
   (name "chameleon-mkl-mt")
   (arguments
    (substitute-keyword-arguments (package-arguments chameleon)
                                  ((#:configure-flags flags '())
                                   `(cons "-DBLA_VENDOR=Intel10_64lp" (cons "-DCHAMELEON_KERNELS_MT=ON" ,flags)))))
   (inputs
    (modify-inputs (package-inputs chameleon)
                   (replace "openblas" mkl-2019)))))

(define-public chameleon+mkl+mt+nompi
  (package
   (inherit chameleon+mkl+mt)
   (name "chameleon-mkl-mt-nompi")
   (arguments
    (substitute-keyword-arguments (package-arguments chameleon+mkl+mt)
                                  ((#:configure-flags flags '())
                                   `(delete "-DCHAMELEON_USE_MPI=ON" ,flags))))))


(define-public chameleon+cuda+mkl+mt
  (package
   (inherit chameleon+mkl+mt)
   (name "chameleon-cuda-mkl-mt")
   (arguments
    (substitute-keyword-arguments (package-arguments chameleon+mkl+mt)
                                  ((#:configure-flags flags '())
                                   `(cons "-DCHAMELEON_USE_CUDA=ON" ,flags))))
   (native-inputs
    (modify-inputs (package-native-inputs chameleon+mkl+mt)
                   (prepend ;; ("gcc" ,gcc-8)                             ;CUDA requires GCC <= 8
                    )))
   (propagated-inputs
    (modify-inputs (package-propagated-inputs chameleon+mkl+mt)
                   (prepend cuda starpu+cuda openmpi-cuda)
                   (delete "starpu")
                   (delete "openmpi")))))

(define-public starpu-example-cppgemm+cuda
  (package
   (inherit starpu-example-cppgemm)
   (name "starpu-example-cppgemm-cuda")
   (synopsis "Example showing how to use starpu for implementing a distributed
gemm in C++, with Cuda support.")
   (arguments
    (substitute-keyword-arguments (package-arguments starpu-example-cppgemm)
                                  ((#:configure-flags flags '())
                                   `(cons "-DENABLE_CUDA=ON" ,flags))))
   (propagated-inputs
    (modify-inputs (package-propagated-inputs starpu-example-cppgemm)
                   (prepend cuda starpu+cuda openmpi-cuda)
                   (delete "starpu")
                   (delete "openmpi")))))

(define-public chameleon+cuda+mkl+mt+nompi
  (package
   (inherit chameleon+cuda+mkl+mt)
   (name "chameleon-cuda-mkl-mt-nompi")
   (arguments
    (substitute-keyword-arguments (package-arguments chameleon+cuda+mkl+mt)
                                  ((#:configure-flags flags '())
                                   `(delete "-DCHAMELEON_USE_MPI=ON" ,flags))))))

(define-public pastix+cuda
  (package
   (inherit pastix)
   (name "pastix-cuda")
   (arguments
    (substitute-keyword-arguments (package-arguments pastix)
                                  ((#:configure-flags flags '())
                                   `(cons "-DPASTIX_WITH_CUDA=ON" ,flags))))
   (native-inputs
    (modify-inputs (package-native-inputs pastix)
                   (prepend ;; ("gcc" ,gcc-8)                             ;CUDA requires GCC <= 8
                    )))
   (propagated-inputs
    (modify-inputs (package-propagated-inputs pastix)
                   (prepend cuda starpu+cuda openmpi-cuda)
                   (delete "starpu")
                   (delete "openmpi")))))

(define-public pastix-mkl-5
  (package
    (inherit pastix-5)
    (name "pastix-mkl")
    (inputs (modify-inputs (package-inputs pastix-5)
              (replace "openblas" mkl)))
    (arguments
     (substitute-keyword-arguments (package-arguments pastix-5)
       ((#:phases phases)
        #~(modify-phases #$phases
            (replace 'configure
              (lambda* (#:key inputs #:allow-other-keys)
                (call-with-output-file "config.in"
                  (lambda (port)
                    (format port
                     "
HOSTARCH    = i686_pc_linux
VERSIONBIT  = _32bit
EXEEXT      =
OBJEXT      = .o
LIBEXT      = .a
CCPROG      = gcc -Wall -g
CFPROG      = gfortran -g
CF90PROG    = gfortran -ffree-form -g
CXXPROG     = g++ -g

MCFPROG     = mpif90 -g
CF90CCPOPT  = -ffree-form -x f95-cpp-input -fallow-argument-mismatch
# Compilation options for optimization (make expor)
CCFOPT      = -O3
# Compilation options for debug (make | make debug)
CCFDEB      = -g3
CXXOPT      = -O3
NVCCOPT     = -O3

LKFOPT      =
MKPROG      = make
MPCCPROG    = mpicc -Wall
MPCXXPROG   = mpic++ -Wall
CPP         = cpp
ARFLAGS     = ruv
ARPROG      = ar
EXTRALIB    = -lgfortran -lm -lrt
CTAGSPROG   = ctags

VERSIONMPI  = _mpi
VERSIONSMP  = _smp
VERSIONSCH  = _static
VERSIONINT  = _int
VERSIONPRC  = _simple
VERSIONFLT  = _real
VERSIONORD  = _scotch

###################################################################
#                  SETTING INSTALL DIRECTORIES                    #
###################################################################
# ROOT          = /path/to/install/directory
# INCLUDEDIR    = ${ROOT}/include
# LIBDIR        = ${ROOT}/lib
# BINDIR        = ${ROOT}/bin
# PYTHON_PREFIX = ${ROOT}

###################################################################
#                  SHARED LIBRARY GENERATION                      #
###################################################################
SHARED=1
SOEXT=.so
SHARED_FLAGS =  -shared -Wl,-soname,__SO_NAME__
CCFDEB       := ${CCFDEB} -fPIC
CCFOPT       := ${CCFOPT} -fPIC
CFPROG       := ${CFPROG} -fPIC

###################################################################
#                          INTEGER TYPE                           #
###################################################################
# Uncomment the following lines for integer type support (Only 1)

#VERSIONINT  = _long
#CCTYPES     = -DFORCE_LONG -DINTSIZELONG
#---------------------------
VERSIONINT  = _int32
CCTYPES     = -DINTSIZE32
#---------------------------
#VERSIONINT  = _int64
#CCTYPES     = -DINTSSIZE64

###################################################################
#                           FLOAT TYPE                            #
###################################################################
CCTYPESFLT  =
# Uncomment the following lines for double precision support
VERSIONPRC  = _double
CCTYPESFLT := $(CCTYPESFLT) -DPREC_DOUBLE

# Uncomment the following lines for float=complex support
VERSIONFLT  = _complex
CCTYPESFLT := $(CCTYPESFLT) -DTYPE_COMPLEX

###################################################################
#                          FORTRAN MANGLING                       #
###################################################################
#CCTYPES    := $(CCTYPES) -DPASTIX_FM_NOCHANGE

###################################################################
#                          MPI/THREADS                            #
###################################################################

# Uncomment the following lines for sequential (NOMPI) version
#VERSIONMPI  = _nompi
#CCTYPES    := $(CCTYPES) -DFORCE_NOMPI
#MPCCPROG    = $(CCPROG)
#MCFPROG     = $(CFPROG)
#MPCXXPROG   = $(CXXPROG)

# Uncomment the following lines for non-threaded (NOSMP) version
#VERSIONSMP  = _nosmp
#CCTYPES    := $(CCTYPES) -DFORCE_NOSMP

# Uncomment the following line to enable a progression thread,
#  then use IPARM_THREAD_COMM_MODE
#CCPASTIX   := $(CCPASTIX) -DPASTIX_THREAD_COMM

# Uncomment the following line if your MPI doesn't support MPI_THREAD_MULTIPLE,
# level then use IPARM_THREAD_COMM_MODE
#CCPASTIX   := $(CCPASTIX) -DPASTIX_FUNNELED

# Uncomment the following line if your MPI doesn't support MPI_Datatype
# correctly
#CCPASTIX   := $(CCPASTIX) -DNO_MPI_TYPE

# Uncomment the following line if you want to use semaphore barrier
# instead of MPI barrier (with IPARM_AUTOSPLIT_COMM)
#CCPASTIX    := $(CCPASTIX) -DWITH_SEM_BARRIER

# Uncomment the following lines to enable StarPU.
#CCPASTIX   := $(CCPASTIX) `pkg-config libstarpu --cflags` -DWITH_STARPU
#EXTRALIB   := $(EXTRALIB) `pkg-config libstarpu --libs`
# Uncomment the correct 2 lines
#CCPASTIX   := $(CCPASTIX) -DCUDA_SM_VERSION=11
#NVCCOPT    := $(NVCCOPT) -maxrregcount 32 -arch sm_11
#CCPASTIX   := $(CCPASTIX) -DCUDA_SM_VERSION=13
#NVCCOPT    := $(NVCCOPT) -maxrregcount 32 -arch sm_13
CCPASTIX   := $(CCPASTIX) -DCUDA_SM_VERSION=20
NVCCOPT    := $(NVCCOPT) -arch sm_20

# Uncomment the following line to enable StarPU profiling
# ( IPARM_VERBOSE > API_VERBOSE_NO ).
#CCPASTIX   := $(CCPASTIX) -DSTARPU_PROFILING

# Uncomment the following line to enable CUDA (StarPU)
#CCPASTIX   := $(CCPASTIX) -DWITH_CUDA

###################################################################
#                          Options                                #
###################################################################

# Show memory usage statistics
CCPASTIX   := $(CCPASTIX) -DMEMORY_USAGE

# Show memory usage statistics in solver
#CCPASTIX   := $(CCPASTIX) -DSTATS_SOPALIN

# Uncomment following line for dynamic thread scheduling support
#CCPASTIX   := $(CCPASTIX) -DPASTIX_DYNSCHED

# Uncomment the following lines for Out-of-core
#CCPASTIX   := $(CCPASTIX) -DOOC -DOOC_NOCOEFINIT -DOOC_DETECT_DEADLOCKS

###################################################################
#                      GRAPH PARTITIONING                         #
###################################################################

# Uncomment the following lines for using metis ordering
#VERSIONORD  = _metis
#METIS_HOME  = ${HOME}/metis-4.0
#CCPASTIX   := $(CCPASTIX) -DMETIS -I$(METIS_HOME)/Lib
#EXTRALIB   := $(EXTRALIB) -L$(METIS_HOME) -lmetis

# Scotch always needed to compile
SCOTCH_HOME  = ~a
SCOTCH_INC  ?= $(SCOTCH_HOME)/include
SCOTCH_LIB  ?= $(SCOTCH_HOME)/lib
# Uncomment on of this blocks
#scotch
CCPASTIX   := $(CCPASTIX) -I$(SCOTCH_INC) -DWITH_SCOTCH
EXTRALIB   := $(EXTRALIB) -L$(SCOTCH_LIB) -lscotch -lscotcherrexit
#ptscotch
#CCPASTIX   := $(CCPASTIX) -I$(SCOTCH_INC) -DDISTRIBUTED -DWITH_SCOTCH
#if scotch >= 6.0
#EXTRALIB   := $(EXTRALIB) -L$(SCOTCH_LIB) -lptscotch -lscotch -lptscotcherrexit
#else
#EXTRALIB   := $(EXTRALIB) -L$(SCOTCH_LIB) -lptscotch -lptscotcherrexit

###################################################################
#                Portable Hardware Locality                       #
###################################################################
# By default PaStiX uses hwloc to bind threads,
# comment this lines if you don't want it (not recommended)
HWLOC_HOME  = ~a
HWLOC_INC  ?= $(HWLOC_HOME)/include
HWLOC_LIB  ?= $(HWLOC_HOME)/lib
CCPASTIX   := $(CCPASTIX) -I$(HWLOC_INC) -DWITH_HWLOC
EXTRALIB   := $(EXTRALIB) -L$(HWLOC_LIB) -lhwloc

###################################################################
#                             MARCEL                              #
###################################################################

# Uncomment following lines for marcel thread support
#VERSIONSMP := $(VERSIONSMP)_marcel
#CCPASTIX   := $(CCPASTIX) `pm2-config --cflags`
#CCPASTIX   += -I${PM2_ROOT}/marcel/include/pthread
#EXTRALIB   := $(EXTRALIB) `pm2-config --libs`
# ---- Thread Posix ------
EXTRALIB   := $(EXTRALIB) -lpthread

# Uncomment following line for bubblesched framework support
# (need marcel support)
#VERSIONSCH  = _dyn
#CCPASTIX   := $(CCPASTIX) -DPASTIX_BUBBLESCHED

###################################################################
#                              BLAS                               #
###################################################################

# Choose Blas library (Only 1)
# Do not forget to set BLAS_HOME if it is not in your environnement
BLAS_HOME = ~a
#----  Blas    ----
#BLASLIB  = -lopenblas
#---- Gotoblas ----
#BLASLIB  = -L${BLAS_HOME} -lgoto
#----  MKL     ----
BLASLIB  = -L$(BLAS_HOME)/lib -Wl,--no-as-needed -lmkl_intel_lp64
BLASLIB += -lmkl_gnu_thread -lmkl_core -lgomp -lpthread -lm -ldl
#----  Acml    ----
#BLASLIB  = -L$(BLAS_HOME) -lacml

###################################################################
#                             MURGE                               #
###################################################################
# Uncomment if you need MURGE interface to be thread safe
# CCPASTIX   := $(CCPASTIX) -DMURGE_THREADSAFE
# Uncomment this to have more timings inside MURGE
# CCPASTIX   := $(CCPASTIX) -DMURGE_TIME

###################################################################
#                          DO NOT TOUCH                           #
###################################################################

FOPT      := $(CCFOPT)
FDEB      := $(CCFDEB)
CCHEAD    := $(CCPROG) $(CCTYPES) $(CCFOPT)
CCFOPT    := $(CCFOPT) $(CCTYPES) $(CCPASTIX)
CCFDEB    := $(CCFDEB) $(CCTYPES) $(CCPASTIX)
NVCCOPT   := $(NVCCOPT) $(CCTYPES) $(CCPASTIX)

###################################################################
#                        MURGE COMPATIBILITY                      #
###################################################################

MAKE     = $(MKPROG)
CC       = $(MPCCPROG)
CFLAGS   = $(CCFOPT) $(CCTYPESFLT)
FC       = $(MCFPROG)
FFLAGS   = $(CCFOPT)
LDFLAGS  = $(EXTRALIB) $(BLASLIB)
CTAGS    = $(CTAGSPROG)
"
                     (assoc-ref inputs "scotch32")
                     (assoc-ref inputs "hwloc")
                     (assoc-ref inputs "mkl"))))))))))
    (synopsis "Sparse matrix direct solver (using Intel MKL)")))

(define-public pastix-5-mkl
  (deprecated-package "pastix-5-mkl" pastix-mkl-5))

;; Note that both lapack and openblas cover the same features currently in guix.
;; Each of them provides both BLAS and LAPACK.
(define mkl-instead-of-openblas-and-lapack
  (package-input-rewriting `((,openblas . ,mkl)
                             (,mumps-openmpi . ,mumps-mkl-openmpi)
                             (,python-numpy . ,python-numpy-mkl)
                             (,lapack . ,mkl))))

(define-public maphys++-mkl
  (package
   (inherit (mkl-instead-of-openblas-and-lapack maphys++))
   (name "maphys++-mkl")))

(define-public fmr
  (package
   (name "fmr")
   (version "0.2")
   (home-page "https://gitlab.inria.fr/compose/legacystack/fmr")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url home-page)
                  (commit "ef765ffa30cee73b1cdcc3925155314eb2cb8853")
                  (recursive? #t)))
            (file-name (string-append name "-" version "-checkout"))
            (sha256
             (base32
              "08i5i0nzimrpm0c02b4yvw0inxsni5ziqp5zg6zsr1zsnqhhjbh2"))))
   (build-system cmake-build-system)
   (arguments
    '(#:configure-flags '("-DBUILD_SHARED_LIBS=ON"
                          "-DCMAKE_NO_SYSTEM_FROM_IMPORTED=ON"
                          "-DFMR_BUILD_TESTS=ON"
                          "-DFMR_USE_HDF5=ON")
                        ;; FIXME: trouble with STARPU /tmp dir.
                        #:tests? #f))

   (inputs (list zlib bzip2 hdf5-1.10 mkl-2019))
   (native-inputs (list pkg-config gfortran))
   (synopsis "Fast and accurate Methods for Randomized numerical linear algebra")
   (description
    "This project provides routines for performing low-rank matrix
approximations based on randomized techniques.")
   (license license:cecill-c)))

(define-public fmr+chameleon+nompi
  (package
   (inherit fmr)
   (name "fmr-chameleon-nompi")
   (arguments
    (substitute-keyword-arguments (package-arguments fmr)
                                  ((#:configure-flags flags '())
                                   `(cons "-DFMR_USE_CHAMELEON=ON" (cons "-DFMR_USE_MPI=OFF" ,flags)))))
   (inputs (modify-inputs (package-inputs fmr)
                          (prepend chameleon+mkl+mt+nompi)))))

;; FIXME: hdf5 delete in package inputs do not work
;;(define-public fmr+mpi
;;  (package
;;    (inherit fmr)
;;    (name "fmr-mpi")
;;    (arguments
;;     (substitute-keyword-arguments (package-arguments fmr)
;;                                   ((#:configure-flags flags '())
;;                                    `(cons "-DFMR_USE_CHAMELEON=ON" ,flags))))
;;    (inputs `(("chameleon" ,chameleon+mkl+mt)
;;              ("hdf5" ,hdf5-parallel-openmpi)
;;              ,@(delete `("hdf5" ,hdf5-1.10) (package-inputs fmr))))))

(define-public fmr+mpi
  (package
   (name "fmr-mpi")
   (version "0.2")
   (home-page "https://gitlab.inria.fr/compose/legacystack/fmr")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url home-page)
                  (commit "ef765ffa30cee73b1cdcc3925155314eb2cb8853")
                  (recursive? #t)))
            (file-name (string-append name "-" version "-checkout"))
            (sha256
             (base32
              "08i5i0nzimrpm0c02b4yvw0inxsni5ziqp5zg6zsr1zsnqhhjbh2"))))
   (build-system cmake-build-system)
   (arguments
    '(#:configure-flags '("-DBUILD_SHARED_LIBS=ON"
                          "-DCMAKE_NO_SYSTEM_FROM_IMPORTED=ON"
                          "-DFMR_BUILD_TESTS=ON"
                          "-DFMR_USE_HDF5=ON"
                          "-DFMR_USE_CHAMELEON=ON")
                        ;; FIXME: trouble with STARPU /tmp dir.
                        #:tests? #f))

   (inputs (list zlib bzip2 hdf5-parallel-openmpi mkl-2019 chameleon+mkl+mt))
   (native-inputs (list pkg-config gfortran openssh))
   (synopsis "Fast and accurate Methods for Randomized numerical linear algebra")
   (description
    "This project provides routines for performing low-rank matrix
approximations based on randomized techniques.")
   (license license:cecill-c)))

(define-public cppdiodon
  (package
   (name "cppdiodon")
   (version "0.2")
   (home-page "https://gitlab.inria.fr/diodon/cppdiodon")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url home-page)
                  (commit "7d0d7ba7943e952dc1113e1d405a49bfc1c5d24b")
                  (recursive? #t)))
            (file-name (string-append name "-" version "-checkout"))
            (sha256
             (base32
              "1ghkk8ihcib2w2sp3llwvz5m9j4h6xf80a026df0p55kv3l6c2ry"))))
   (build-system cmake-build-system)
   (arguments
    '(#:configure-flags `("-DBUILD_SHARED_LIBS=ON"
                          "-DCMAKE_NO_SYSTEM_FROM_IMPORTED=ON"
                          "-DDIODON_USE_INTERNAL_FMR=OFF")
                        ;; FIXME: trouble with STARPU /tmp dir.
                        #:tests? #f))

   (inputs (list zlib bzip2 hdf5-1.10 mkl-2019 fmr))
   (native-inputs (list pkg-config gfortran))

   (synopsis "Librairies for Multivariate Data Analysis and
Dimensionality Reduction for very large datasets")
   (description
    "Librairies for Multivariate Data Analysis and Dimensionality
Reduction for very large datasets.")
   (license license:cecill-c)))

(define-public cppdiodon+chameleon+nompi
  (package
   (inherit cppdiodon)
   (name "cppdiodon-chameleon-nompi")
   (arguments
    (substitute-keyword-arguments (package-arguments cppdiodon)
                                  ((#:configure-flags flags '())
                                   `(cons "-DDIODON_USE_CHAMELEON=ON" (cons "-DDIODON_USE_CHAMELEON_MPI=OFF" ,flags)))))
   (inputs `(("chameleon" ,chameleon+mkl+mt+nompi)
             ("fmr" ,fmr+chameleon+nompi)
             ,@(delete `("fmr" ,fmr) (package-inputs cppdiodon))))))

;; FIXME: fmr and hdf5 delete in package inputs do not work
;; (define-public cppdiodon+mpi
;;   (package
;;     (inherit cppdiodon)
;;     (name "cppdiodon-mpi")
;;     (arguments
;;      (substitute-keyword-arguments (package-arguments cppdiodon)
;;                                    ((#:configure-flags flags '())
;;                                     `(cons "-DDIODON_USE_CHAMELEON=ON" ,flags))))
;;     (inputs `(("chameleon" ,chameleon+mkl+mt)
;;               ("fmr" ,fmr+mpi)
;;               ("hdf5" ,hdf5-parallel-openmpi)
;;               ,@(delete `("fmr" ,fmr) (package-inputs cppdiodon))
;;               ,@(delete `("hdf5" ,hdf5-1.10) (package-inputs cppdiodon))))))

(define-public cppdiodon+mpi
  (package
   (name "cppdiodon-mpi")
   (version "0.2")
   (home-page "https://gitlab.inria.fr/diodon/cppdiodon")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url home-page)
                  (commit "7d0d7ba7943e952dc1113e1d405a49bfc1c5d24b")
                  (recursive? #t)))
            (file-name (string-append name "-" version "-checkout"))
            (sha256
             (base32
              "1ghkk8ihcib2w2sp3llwvz5m9j4h6xf80a026df0p55kv3l6c2ry"))))
   (build-system cmake-build-system)
   (arguments
    '(#:configure-flags `("-DBUILD_SHARED_LIBS=ON"
                          "-DCMAKE_NO_SYSTEM_FROM_IMPORTED=ON"
                          "-DDIODON_USE_INTERNAL_FMR=OFF"
                          "-DDIODON_USE_CHAMELEON=ON")
                        ;; FIXME: trouble with STARPU /tmp dir.
                        #:tests? #f))

   (inputs (list zlib
                 bzip2
                 hdf5-parallel-openmpi
                 mkl-2019
                 chameleon+mkl+mt
                 fmr+mpi))
   (native-inputs (list pkg-config gfortran openssh))

   (synopsis "Librairies for Multivariate Data Analysis and
Dimensionality Reduction for very large datasets")
   (description
    "Librairies for Multivariate Data Analysis and Dimensionality
Reduction for very large datasets.")
   (license license:cecill-c)))

(define-public python-cppdiodon
  (package
   (name "python-cppdiodon")
   (version "0.2")
   (home-page "https://gitlab.inria.fr/diodon/cppdiodon")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url home-page)
                  (commit "7d0d7ba7943e952dc1113e1d405a49bfc1c5d24b")
                  (recursive? #t)))
            (file-name (string-append name "-" version "-checkout"))
            (sha256
             (base32
              "1ghkk8ihcib2w2sp3llwvz5m9j4h6xf80a026df0p55kv3l6c2ry"))))
   (build-system python-build-system)
   (arguments
    '(#:phases (modify-phases %standard-phases
                              (add-before 'build 'change-directory
                                          (lambda _
                                            (setenv "CMAKE_ARGS" "-DCMAKE_NO_SYSTEM_FROM_IMPORTED=ON")
                                            (chdir "python")
                                            #t)))))

   (propagated-inputs (list python-numpy mkl-2019 libomp))
   (inputs (list cppdiodon fmr zlib bzip2 hdf5-1.10))
   (native-inputs (list cmake gfortran pkg-config pybind11))
   (synopsis "Librairies for Multivariate Data Analysis and
Dimensionality Reduction for very large datasets")
   (description
    "Librairies for Multivariate Data Analysis and Dimensionality
Reduction for very large datasets.")
   (license license:cecill-c)))

(define-public python-cppdiodon-chameleon
  (package
   (inherit python-cppdiodon)
   (name "python-cppdiodon-chameleon")
   (inputs `(("chameleon" ,chameleon+mkl+mt+nompi)
             ("fmr" ,fmr+chameleon+nompi)
             ("cppdiodon" ,cppdiodon+chameleon+nompi)
             ,@(delete `("fmr" ,fmr) (package-inputs python-cppdiodon))
             ,@(delete `("cppdiodon" ,cppdiodon) (package-inputs python-cppdiodon))))))

(define-public python-pydiodon
  (package
   (name "python-pydiodon")
   (version "0.0.4")
   (home-page "https://gitlab.inria.fr/diodon/pydiodon")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url home-page)
                  (commit "30b8b4f4cfda059ab50264e189b3729ab91f217d")
                  (recursive? #t)))
            (file-name (string-append name "-" version "-checkout"))
            (sha256
             (base32
              "1sia2xkank3vxi7x21a128wbsjs3l33rdymicq35yw4mn67x6iv0"))))
   (build-system python-build-system)
   (propagated-inputs (list python-h5py python-matplotlib python-numpy python-scipy))
   (synopsis "Librairies for Multivariate Data Analysis and
Dimensionality Reduction for very large datasets")
   (description
    "Librairies for Multivariate Data Analysis and Dimensionality
Reduction for very large datasets.")
   (license license:cecill-c)))
